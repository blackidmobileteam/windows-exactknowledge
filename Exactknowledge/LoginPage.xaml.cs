﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.Net;
using System.Net.Http;
using Windows.Data.Json;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Exactknowledge
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class LoginPage : Page
    {

        private HttpClient httpClient;

        public LoginPage()
        {
            this.InitializeComponent();

            httpClient = new HttpClient();
            httpClient.MaxResponseContentBufferSize = 256000;
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            if (Username.Text == "" && Password.Password == "")
            {
                ErrorMsg.Text = "Please Enter Username and Password";
                ErrorMsg.Visibility = Windows.UI.Xaml.Visibility.Visible;
            }
            else if (Username.Text == "")
            {
                    ErrorMsg.Text = "Please Enter Username";
                    ErrorMsg.Visibility = Windows.UI.Xaml.Visibility.Visible;
            }
            else if (Password.Password == "")
            {
                    ErrorMsg.Text = "Please Enter Password";
                    ErrorMsg.Visibility = Windows.UI.Xaml.Visibility.Visible;
            }
            else
            {
                try
                {
                    status.Text = "Waiting for response ...";
                    var client = new HttpClient();
                    var postData = new List<KeyValuePair<string, string>>();
                    postData.Add(new KeyValuePair<string, string>("username", Username.Text));
                    postData.Add(new KeyValuePair<string, string>("password", Password.Password));

                    HttpContent resultContent = new FormUrlEncodedContent(postData);

                    var response    = await client.PostAsync("http://192.168.1.17/exact_knowledge_new/services/gets.php?req=login", resultContent);
                    var content     = await response.Content.ReadAsStringAsync();

                    JsonObject obj = JsonObject.Parse(content);

                    String statusResponse = obj.GetNamedString("status");

                    if (statusResponse.Equals("success"))
                    {
                        status.Text = "login successfully";
                        this.Frame.Navigate(typeof(HomeScreen));
                    }
                    else
                    {
                        status.Text = "login failed";
                    }

                }
                catch (HttpRequestException hre)
                {
                    status.Text = hre.ToString();
                }
                catch (Exception ex)
                {
                    // For debugging
                    status.Text = ex.ToString();
                }
            }
            
        }

        private void OnClick(object sender, KeyRoutedEventArgs e)
        {
            Frame rootFrame = Window.Current.Content as Frame;

            if (rootFrame != null && rootFrame.CanGoBack)
            {
                e.Handled = true;
                rootFrame.GoBack();
            }
        }

    }
}
